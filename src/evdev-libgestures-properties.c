#include <gestures/gestures.h>

#include "evdev-libgestures.h"

// Generic mouse settings:
//   "Mouse Accel Curves" 1
//   "Mouse Scroll Curves" 1
//   "Accel Min dt" 0.0001

static void Prop_RegisterHandlers(void* priv, GesturesProp* prop,
                                  void* handler_data,
                                  GesturesPropGetHandler get,
                                  GesturesPropSetHandler set)
{
    // printf("RegisterHandlers(%p, %p, %p)\n", prop, get, set);

    // Sanity checks
    if (!priv || !prop)
        return;

    prop->handler_data = handler_data;
    prop->get = get;
    prop->set = set;
}

static GesturesProp* PropCreate_Int(void* priv, const char* name, int* val, size_t count, const int* init)
{
    GesturesProp *prop = zalloc(sizeof(GesturesProp));

    for (int i = 0; i < count; i++) {
        val[i] = init[i];
    }

    return prop;
}

static GesturesProp* PropCreate_Short(void* priv, const char* name, short* val, size_t count,
                                      const short* init)
{
    GesturesProp *prop = zalloc(sizeof(GesturesProp));

    if (strcmp(name, "Touchpad Stack Version") == 0) { *val = 2; return prop; }

    for (int i = 0; i < count; i++) {
        val[i] = init[i];
    }

    return prop;
}

static GesturesProp* PropCreate_Bool(void* priv, const char* name, GesturesPropBool* val,
                                     size_t count, const GesturesPropBool* init)
{
    GesturesProp *prop = zalloc(sizeof(GesturesProp));

    printf("%s = ", name);

    for (int i = 0; i < count; i++) {
        val[i] = init[i];
    }

    // if (strcmp(name, "Mouse Accel Curves") == 0 || strcmp(name, "Mouse Scroll Curves") == 0) { *val = 1; }
    if (strcmp(name, "Integrated Touchpad") == 0) { *val = 1; }
    if (strcmp(name, "Filter Low Pressure") == 0) { *val = 1; }
    if (strcmp(name, "Tap Enable") == 0) { *val = 0; }

    for (int i = 0; i < count; i++) {
        printf("%d ", val[i]);
    }

    printf("\n");

    //  {
    //     *val = 1;
    // } else {
    
    // }



    return prop;
}

static GesturesProp* PropCreate_String(void* priv, const char* name, const char** val,
                                       const char* init)
{
    GesturesProp *prop = zalloc(sizeof(GesturesProp));

    return prop;
}

static GesturesProp* PropCreate_Real(void* priv, const char* name, double* val, size_t count,
                                     const double* init)
{
    GesturesProp *prop = zalloc(sizeof(GesturesProp));

    printf("%s = ", name);

    for (int i = 0; i < count; i++) {
        val[i] = init[i];
    }

    if (strcmp(name, "Pressure Calibration Offset") == 0) { *val = -8.345256186; }
    if (strcmp(name, "Pressure Calibration Slope") == 0) { *val = -1.006531862; }
    if (strcmp(name, "Tap Minimum Pressure") == 0) { *val = -15.0; }

    for (int i = 0; i < count; i++) {
        printf("%f ", val[i]);
    }

    printf("\n");

    return prop;
}

static void Prop_Free(void* priv, GesturesProp* prop)
{
    free(prop);
}

GesturesPropProvider libgestures_dispatch_PropProvider = {
    PropCreate_Int,
    PropCreate_Short,
    PropCreate_Bool,
    PropCreate_String,
    PropCreate_Real,
    Prop_RegisterHandlers,
    Prop_Free
};
