#include "config.h"

#include <mtdev-plumbing.h>
#include <gestures/gestures.h>

#include "evdev-libgestures.h"

#define EVDEV_BUTTON_MAP_SIZE 7
static const int kEvdevButtonMap[EVDEV_BUTTON_MAP_SIZE][2] = {
    {BTN_LEFT, GESTURES_BUTTON_LEFT},
    {BTN_MIDDLE, GESTURES_BUTTON_MIDDLE},
    {BTN_RIGHT, GESTURES_BUTTON_RIGHT},
    {BTN_BACK, GESTURES_BUTTON_BACK},
    {BTN_SIDE, GESTURES_BUTTON_BACK},
    {BTN_FORWARD, GESTURES_BUTTON_FORWARD},
    {BTN_EXTRA, GESTURES_BUTTON_FORWARD}
};

static inline void
normalize_delta(struct evdev_device *device,
		const struct device_coords *delta,
		struct normalized_coords *normalized)
{
	normalized->x = delta->x * DEFAULT_MOUSE_DPI / (double)device->dpi;
	normalized->y = delta->y * DEFAULT_MOUSE_DPI / (double)device->dpi;
}

static inline void
libgestures_process_key(struct libgestures_dispatch *dispatch,
		     struct evdev_device *device,
		     struct input_event *e, uint64_t time)
{
	/* ignore kernel key repeat */
	if (e->value == 2)
		return;

	if (get_key_type(e->code) == KEY_TYPE_BUTTON) {
		switch (e->code) {
		case BTN_TOUCH:
			if (e->value)
				dispatch->hwstate.touch_cnt = 1;
			break;
		case BTN_TOOL_FINGER:
			if (e->value)
				dispatch->hwstate.touch_cnt = 1;
			break;
		case BTN_TOOL_DOUBLETAP:
			if (e->value)
				dispatch->hwstate.touch_cnt = 2;
			break;
		case BTN_TOOL_TRIPLETAP:
			if (e->value)
				dispatch->hwstate.touch_cnt = 3;
			break;
		case BTN_TOOL_QUADTAP:
			if (e->value)
				dispatch->hwstate.touch_cnt = 4;
			break;
		case BTN_TOOL_QUINTTAP:
			if (e->value)
				dispatch->hwstate.touch_cnt = 5;
			break;
		}
		for (int i = 0; i < EVDEV_BUTTON_MAP_SIZE; ++i) {
			if (e->code == kEvdevButtonMap[i][0]) {

				if ((e->value && hw_is_key_down(dispatch, e->code)) ||
						(e->value == 0 && !hw_is_key_down(dispatch, e->code)))
					continue;

				if (e->value) {
					dispatch->hwstate.buttons_down |= kEvdevButtonMap[i][1];
				} else {
					dispatch->hwstate.buttons_down ^= kEvdevButtonMap[i][1];
				}

				hw_set_key_down(dispatch, e->code, e->value);
			}
    	}
	}
}

static inline bool
libgestures_reject_relative(struct evdev_device *device,
			 const struct input_event *e,
			 uint64_t time)
{
	if ((e->code == REL_X || e->code == REL_Y) &&
	    (device->seat_caps & EVDEV_DEVICE_POINTER) == 0) {
		evdev_log_bug_libinput_ratelimit(device,
						 &device->nonpointer_rel_limit,
						 "REL_X/Y from a non-pointer device\n");
		return true;
	}

	return false;
}

static inline void
libgestures_process_relative(struct libgestures_dispatch *dispatch,
			  struct evdev_device *device,
			  struct input_event *e, uint64_t time)
{
	if (libgestures_reject_relative(device, e, time))
		return;

	switch (e->code) {
	case REL_X:
		dispatch->hwstate.rel_x += e->value;
		break;
	case REL_Y:
		dispatch->hwstate.rel_y += e->value;
		break;
	case REL_WHEEL:
		dispatch->hwstate.rel_wheel += e->value;
		break;
	case REL_HWHEEL:
		dispatch->hwstate.rel_hwheel += e->value;
		break;
	}
}

static void
libgestures_handle_state(struct libgestures_dispatch *dispatch,
		      struct evdev_device *device,
		      uint64_t time)
{
	stime_t t = (stime_t)time / 1000000.0;

	dispatch->hwstate.timestamp = t;

	memset(dispatch->hwstate.fingers, 0, 10 * sizeof (struct FingerState));

	int current_finger = 0;
	for (int i = 0; i < dispatch->mt_slots_len; i++) {
		if (!dispatch->mt_slots[i].active) {
			continue;
		}

		struct libgestures_mt_slot *slot = &dispatch->mt_slots[i];
		struct FingerState *finger = &dispatch->hwstate.fingers[current_finger];

		finger->position_x  = slot->position_x;
		finger->position_y  = slot->position_y;
		finger->pressure    = slot->pressure;
		finger->width_major = slot->width_major;
		finger->width_minor = slot->width_minor;
		finger->touch_minor = slot->touch_minor;
		finger->touch_major = slot->touch_major;
		finger->tracking_id = slot->tracking_id;
		current_finger++;
	}
	dispatch->hwstate.finger_cnt = current_finger;

	GestureInterpreterPushHardwareState(dispatch->interp, &dispatch->hwstate);

	dispatch->hwstate.touch_cnt = 0;
	dispatch->hwstate.rel_x = 0;
	dispatch->hwstate.rel_y = 0;
	dispatch->hwstate.rel_wheel = 0;
	dispatch->hwstate.rel_hwheel = 0;
}

static void
libgestures_process_absolute(struct libgestures_dispatch *dispatch,
			struct evdev_device *device,
			struct input_event *event,
			uint64_t time)
{
	if (event->code == ABS_MT_SLOT) {
		if ((size_t)event->value >= dispatch->mt_slots_len) {
			printf(device,
					 "egest: exceeded slot count (%d of max %zd)\n",
					 event->value,
					 dispatch->mt_slots_len);
			event->value = dispatch->mt_slots_len - 1;
		}
		dispatch->mt_slot = event->value;

		return;
	}

	struct libgestures_mt_slot *slot = &dispatch->mt_slots[dispatch->mt_slot];

	switch (event->code) {
	case ABS_MT_TRACKING_ID:
		// printf("  tracking_id = %d\n", event->value);
		slot->tracking_id = event->value;
		
		if (slot->tracking_id >= 0) {
			slot->active = 1;
		} else {
			slot->active = 0;
		}
		//if (event->value >= 0) {
//			dispatch->pending_event |= EVDEV_ABSOLUTE_MT;
//			slot->state = SLOT_STATE_BEGIN;
		//} else {
//			dispatch->pending_event |= EVDEV_ABSOLUTE_MT;
//			slot->state = SLOT_STATE_END;
		//}
//		slot->dirty = true;
		break;
	case ABS_MT_PRESSURE:
		slot->pressure = (float)event->value;
		break;
	case ABS_MT_TOUCH_MAJOR:
		slot->touch_major = (float)event->value;
		break;
	case ABS_MT_TOUCH_MINOR:
		slot->touch_minor = (float)event->value;
		break;
	case ABS_MT_WIDTH_MAJOR:
		slot->width_major = (float)event->value;
		break;
	case ABS_MT_WIDTH_MINOR:
		slot->width_minor = (float)event->value;
		break;
	case ABS_MT_POSITION_X:
		slot->position_x = (float)event->value;
		break;
	case ABS_MT_POSITION_Y:
		slot->position_y = (float)event->value;
		break;
	}

}

static void
libgestures_interface_process(struct evdev_dispatch *evdev_dispatch,
			   struct evdev_device *device,
			   struct input_event *event,
			   uint64_t time)
{
	struct libgestures_dispatch *dispatch = libgestures_dispatch(evdev_dispatch);

	if (dispatch->ignore_events)
		return;

	switch (event->type) {
	case EV_REL:
		libgestures_process_relative(dispatch, device, event, time);
		break;
	case EV_ABS:
		if (device->is_mt)
			libgestures_process_absolute(dispatch, device, event, time);
		break;
	case EV_KEY:
		libgestures_process_key(dispatch, device, event, time);
		break;
	case EV_SW:
		break;
	case EV_SYN:
		libgestures_handle_state(dispatch, device, time);
		break;
	}
}

static void
libgestures_interface_destroy(struct evdev_dispatch *evdev_dispatch)
{
	struct libgestures_dispatch *dispatch = libgestures_dispatch(evdev_dispatch);

	free(dispatch->hwstate.fingers);
	free(dispatch);
}

struct evdev_dispatch_interface libgestures_interface = {
	.process = libgestures_interface_process,
	.suspend = NULL,
	.remove = NULL,
	.destroy = libgestures_interface_destroy,
	.device_added = NULL,
	.device_removed = NULL,
	.device_suspended = NULL,
	.device_resumed = NULL,
	.post_added = NULL,
	.toggle_touch = NULL,
	.get_switch_state = NULL,
};


static void
libgestures_dispatch_Callback(void* client_data, const struct Gesture* gesture)
{
	// printf("Callback(%p, %p)\n", client_data, gesture);

	uint64_t time = (uint64_t)(gesture->start_time * 1000000.0);

	struct libgestures_dispatch *dispatch = client_data;
	struct libinput_device *base = &dispatch->device->base;

    switch (gesture->type) {
        case kGestureTypeMove: {
            const GestureMove *move = &gesture->details.move;
            // printf("egest: Gesture Move: (%f, %f) [%f, %f]\n",
            //        move->dx, move->dy, move->ordinal_dx, move->ordinal_dy);

			struct normalized_coords norm_coords = {
				.x = move->dx,
				.y = move->dy
			};

			struct device_float_coords dev_coords = {
				.x = move->ordinal_dx,
				.y = move->ordinal_dy,
			};

			pointer_notify_motion(base, time, &norm_coords, &dev_coords);

            break;
        }

        case kGestureTypeScroll: {
            const GestureScroll* scroll = &gesture->details.scroll;
            printf("Gesture Scroll: (%f, %f) [%f, %f]\n",
                scroll->dx, scroll->dy, scroll->ordinal_dx, scroll->ordinal_dy);

			struct normalized_coords norm_coords = {
				.y = -scroll->dy,
				.x = scroll->dx,
			};

			evdev_post_scroll(dispatch->device, time,
						LIBINPUT_POINTER_AXIS_SOURCE_CONTINUOUS,
						&norm_coords);
	    }

		case kGestureTypeButtonsChange: {
			const GestureButtonsChange *buttons = &gesture->details.buttons;
			printf("Gesture Button Change: down=0x%02x up=0x%02x\n",
               buttons->down, buttons->up);

			for (int i = 0; i < EVDEV_BUTTON_MAP_SIZE; i++) {
				if (buttons->down & kEvdevButtonMap[i][1]) {
					if (!hw_is_key_down(dispatch, kEvdevButtonMap[i][0]))
						evdev_pointer_notify_physical_button(dispatch->device, time, kEvdevButtonMap[i][0], LIBINPUT_BUTTON_STATE_PRESSED);
				} else if (buttons->up & kEvdevButtonMap[i][1]) {
					if (hw_is_key_down(dispatch, kEvdevButtonMap[i][0]))
						evdev_pointer_notify_physical_button(dispatch->device, time, kEvdevButtonMap[i][0], LIBINPUT_BUTTON_STATE_RELEASED);
				}
			}
		}

        default:
            // printf("Unrecognized gesture type (%u)\n", gesture->type);
            break;
    }
}

struct evdev_dispatch *
libgestures_dispatch_create(struct libinput_device *libinput_device)
{
	struct evdev_device *device = evdev_device(libinput_device);
	struct libgestures_dispatch *dispatch;

	dispatch = zalloc(sizeof *dispatch);
	dispatch->device = evdev_device(libinput_device);
	dispatch->base.dispatch_type = DISPATCH_LIBGESTURES;
	dispatch->base.interface = &libgestures_interface;

	memset(dispatch->hw_key_mask, 0, sizeof(dispatch->hw_key_mask));

	dispatch->interp = NewGestureInterpreter();
	dispatch->hwstate.fingers = zalloc(10 * sizeof(struct FingerState));
	dispatch->mt_slots = zalloc(10 * sizeof(struct libgestures_mt_slot));
	dispatch->mt_slots_len = 10;

    if (dispatch->interp == NULL) {
		free(dispatch);
        return NULL;
    }

	struct HardwareProperties hwprops = { 0 };

	hwprops.res_x = device->dpi;
	hwprops.res_y = device->dpi;
	hwprops.screen_x_dpi = 133;
	hwprops.screen_y_dpi = 133;

	enum GestureInterpreterDeviceClass device_class = GESTURES_DEVCLASS_MOUSE;
	if (device->is_mt) {
		device_class = GESTURES_DEVCLASS_TOUCHPAD;
		hwprops.res_x = (float) dispatch->device->abs.absinfo_x->resolution;
		hwprops.res_y = (float)dispatch->device->abs.absinfo_y->resolution;
		hwprops.max_finger_cnt = 10;
		hwprops.max_touch_cnt = 5;
		hwprops.top = (float)dispatch->device->abs.absinfo_y->minimum;
		hwprops.bottom = (float)dispatch->device->abs.absinfo_y->maximum;
		hwprops.right = (float)dispatch->device->abs.absinfo_x->maximum;
		hwprops.left = (float)dispatch->device->abs.absinfo_x->minimum;
	}

    GestureInterpreterInitialize(dispatch->interp, device_class);
	GestureInterpreterSetHardwareProperties(dispatch->interp, &hwprops);
    GestureInterpreterSetCallback(dispatch->interp, &libgestures_dispatch_Callback, dispatch);
	GestureInterpreterSetTimerProvider(dispatch->interp, &libgestures_dispatch_TimerProvider, dispatch);
	GestureInterpreterSetPropProvider(dispatch->interp, &libgestures_dispatch_PropProvider, NULL);

	return &dispatch->base;
}
