#include "config.h"

#ifndef EVDEV_LIBGESTURES_H
#define EVDEV_LIBGESTURES_H

#include <gestures/gestures.h>

#include "evdev.h"

struct libgestures_mt_slot {
	int active;
	int tracking_id;
	double pressure;
	double position_x, position_y;
	double width_major, width_minor;
	double touch_major, touch_minor;
};

struct libgestures_dispatch {
	struct evdev_dispatch base;
	struct evdev_device *device;

	struct libinput_device_config_calibration calibration;

	/* Bitmask of pressed keys used to ignore initial release events from
	 * the kernel. */
	unsigned long hw_key_mask[NLONGS(KEY_CNT)];
	unsigned long last_hw_key_mask[NLONGS(KEY_CNT)];

	/* true if we're reading events (i.e. not suspended) but we're
	   ignoring them */
	bool ignore_events;

	struct HardwareState hwstate;
	GestureInterpreter *interp;

	struct libgestures_mt_slot *mt_slots;

	int mt_slot;
	int mt_slots_len;	
};

static inline struct libgestures_dispatch*
libgestures_dispatch(struct evdev_dispatch *dispatch)
{
	evdev_verify_dispatch_type(dispatch, DISPATCH_LIBGESTURES);

	return container_of(dispatch, struct libgestures_dispatch, base);
}

enum key_type {
	KEY_TYPE_NONE,
	KEY_TYPE_KEY,
	KEY_TYPE_BUTTON,
};

static inline enum key_type
get_key_type(uint16_t code)
{
	switch (code) {
	case BTN_TOOL_PEN:
	case BTN_TOOL_RUBBER:
	case BTN_TOOL_BRUSH:
	case BTN_TOOL_PENCIL:
	case BTN_TOOL_AIRBRUSH:
	case BTN_TOOL_MOUSE:
	case BTN_TOOL_LENS:
	case BTN_TOOL_QUINTTAP:
	case BTN_TOOL_DOUBLETAP:
	case BTN_TOOL_TRIPLETAP:
	case BTN_TOOL_QUADTAP:
	case BTN_TOOL_FINGER:
	case BTN_TOUCH:
		return KEY_TYPE_NONE;
	}

	if (code >= KEY_ESC && code <= KEY_MICMUTE)
		return KEY_TYPE_KEY;
	if (code >= BTN_MISC && code <= BTN_GEAR_UP)
		return KEY_TYPE_BUTTON;
	if (code >= KEY_OK && code <= KEY_LIGHTS_TOGGLE)
		return KEY_TYPE_KEY;
	if (code >= BTN_DPAD_UP && code <= BTN_DPAD_RIGHT)
		return KEY_TYPE_BUTTON;
	if (code >= KEY_ALS_TOGGLE && code <= KEY_ONSCREEN_KEYBOARD)
		return KEY_TYPE_KEY;
	if (code >= BTN_TRIGGER_HAPPY && code <= BTN_TRIGGER_HAPPY40)
		return KEY_TYPE_BUTTON;
	return KEY_TYPE_NONE;
}

static inline void
hw_set_key_down(struct libgestures_dispatch *dispatch, int code, int pressed)
{
	long_set_bit_state(dispatch->hw_key_mask, code, pressed);
}

static inline bool
hw_key_has_changed(struct libgestures_dispatch *dispatch, int code)
{
	return long_bit_is_set(dispatch->hw_key_mask, code) !=
		long_bit_is_set(dispatch->last_hw_key_mask, code);
}

static inline void
hw_key_update_last_state(struct libgestures_dispatch *dispatch)
{
	static_assert(sizeof(dispatch->hw_key_mask) ==
		      sizeof(dispatch->last_hw_key_mask),
		      "Mismatching key mask size");

	memcpy(dispatch->last_hw_key_mask,
	       dispatch->hw_key_mask,
	       sizeof(dispatch->hw_key_mask));
}

static inline bool
hw_is_key_down(struct libgestures_dispatch *dispatch, int code)
{
	return long_bit_is_set(dispatch->hw_key_mask, code);
}

static inline int
get_key_down_count(struct evdev_device *device, int code)
{
	return device->key_count[code];
}

GesturesTimerProvider libgestures_dispatch_TimerProvider;

struct GesturesTimer {
	struct libinput_timer timer;
	GesturesTimerCallback callback;
	void *callback_data;
};

struct GesturesProp {
	void *handler_data;
	GesturesPropSetHandler set;
    GesturesPropGetHandler get;
};

GesturesPropProvider libgestures_dispatch_PropProvider;

#endif
