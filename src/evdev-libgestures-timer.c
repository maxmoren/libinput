#include <gestures/gestures.h>

#include "evdev-libgestures.h"

static GesturesTimer *
libgestures_dispatch_TimerCreate(void* provider_data)
{
    struct GesturesTimer *timer;

    timer = (GesturesTimer *)zalloc(sizeof(GesturesTimer));

    // DeviceIntPtr dev = provider_data;
    // InputInfoPtr info = dev->public.devicePrivate;
    // CmtDevicePtr cmt = info->private;
    // GesturesTimer* timer = (GesturesTimer*)calloc(1, sizeof(GesturesTimer));
    // if (!timer)
    //     return NULL;
    // timer->timer = TimerSet(NULL, 0, 0, NULL, 0);
    // if (!timer->timer) {
    //     free(timer);
    //     return NULL;
    // }
    // timer->is_monotonic = cmt->evdev.info.is_monotonic;
    // return timer;

    return timer;
}

libgestures_dispatch_TimerCallback(uint64_t now, void *funcdata)
{
    GesturesTimer *timer = funcdata;
    stime_t snow = (stime_t)now / 1000000.0;
    stime_t ret = timer->callback(snow, timer->callback_data);

    if (ret > 0.0) {
        uint64_t expires = now + (uint64_t)(ret * 1000000.0);
        libinput_timer_set(&timer->timer, expires);
    }
}

static void
libgestures_dispatch_TimerSet(void* provider_data,
                 GesturesTimer* timer,
                 stime_t delay,
                 GesturesTimerCallback callback,
                 void* callback_data)
{
    struct libgestures_dispatch *d = provider_data;

    timer->callback = callback;
    timer->callback_data = callback_data;

    struct libinput *li = evdev_libinput_context(d->device); 

    // TODO: Wrap and call callback with correct stime_t = seconds as double
    libinput_timer_init(
                &timer->timer,
			    li,
			    "libgestures_timer",
			    libgestures_dispatch_TimerCallback,
                timer);

    uint64_t now = libinput_now(li);
    uint64_t expires = now + (uint64_t)(delay * 1000000.0);
    libinput_timer_set(&timer->timer, expires);

    // CARD32 ms = delay * 1000.0;
    // if (!timer)
    //     return;
    // timer->callback = callback;
    // timer->callback_data = callback_data;
    // if (ms == 0)
    //     ms = 1;
    // TimerSet(timer->timer, 0, ms, Gesture_TimerCallback, timer);
}

static void
libgestures_dispatch_TimerCancel(void* provider_data, GesturesTimer* timer)
{
    // printf("TimerCancel(%p, %p)\n",
    //     provider_data,
    //     timer
    // );

    libinput_timer_cancel(&timer->timer);
}

static void
libgestures_dispatch_TimerFree(void* provider_data, GesturesTimer* timer)
{
    // printf("TimerFree(%p, %p)\n",
    //     provider_data,
    //     timer
    // );

    libinput_timer_destroy(&timer->timer);
    free(timer);
}

GesturesTimerProvider libgestures_dispatch_TimerProvider = {
    .create_fn = libgestures_dispatch_TimerCreate,
    .set_fn = libgestures_dispatch_TimerSet,
    .cancel_fn = libgestures_dispatch_TimerCancel,
    .free_fn = libgestures_dispatch_TimerFree
};